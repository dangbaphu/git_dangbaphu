<?php 
    include_once './vendor/autoload.php';
    try {
    $conn = new PDO(
        'mysql:host=localhost;dbname=phudb;charset=utf8',
        'root',
        ''
    );

    } catch (PDOException $ex) {
        echo 'Ket noi that bai';
    }
?>
<html>
    <head>
        <title>LoginPdo</title>
        <link rel="stylesheet" type="text/css" href="vendor/twbs/bootstrap/dist/css/bootstrap.min.css">
        <style>
            .input {
                margin-top: 20px;
            }

            .error {
                color: red;
            }
        </style>
    </head>
    <body>
        <?php
            $error = array();
            $data = array();
            if (isset($_POST['login_action'])) {
                $data['mail_address'] = isset($_POST['mail_address']) ? $_POST['mail_address'] : '';
                $data['password'] = isset($_POST['password']) ? $_POST['password'] : '';

                if (empty($data['mail_address'])) {
                    $error['mail_address'] = 'Bạn chưa nhập email';
                } else if (!filter_var($data['mail_address'], FILTER_VALIDATE_EMAIL)) {
                    $error['mail_address'] = 'Email không đúng định dạng';
                } else if (strlen($data['mail_address']) >255) {
                    $error['mail_address'] = 'Độ dài email từ 6 đến 50 ký tự';
                }

                if (empty($data['password'])) {
                    $error['password'] = 'Bạn chưa nhập password';
                } else if (strlen($data['password']) < 6 || strlen($data['password']) > 50) {
                    $error['password'] = 'Độ dài password từ 6 đến 50 ký tự';
                }

                if (!isset($error['mail_address']) && !isset($error['password'])) {
                    $query = "SELECT mail_address FROM users WHERE mail_address = :mail_address AND password = :password";
                    $stmt = $conn->prepare($query);
                    $stmt->bindValue(':mail_address', $data['mail_address']);
                    $stmt->bindValue(':password', md5($data['password']));
                    $stmt->execute();
                    $user = $stmt->fetch(PDO::FETCH_ASSOC);
                    if ($user) {
                        echo "Đăng nhập thành công";
                        session_start();
                        $_SESSION['mail_address'] = $data['mail_address'];
                        if (isset($_POST['remember_me'])) {
                            setcookie("mail_address", $data['mail_address'], time() + 3600*24*100);
                            setcookie("password", $data['password'], time() + 3600*24*100);
                            header("Location: LoginSuccessPdo.php");
                        } else {
                            if (isset($_COOKIE['mail_address']) && isset($_COOKIE['password'])) {
                                setcookie("mail_address", "", time() - 3600*24*100);
                                setcookie("password", "", time() - 3600*24*100);
                                header("Location: LoginSuccessPdo.php");
                            } else {
                                header("Location: LoginSuccessPdo.php");
                            }
                        }
                    } else {
                        $error['password'] = "Đăng nhập thất bại";
                    }
                }
            }
        ?>
        <form method="POST" action="">
            <div class="container">
                <div class="row">
                    <div class="input-group col-md-4 offset-md-4" style="margin-top: 100px">
                        <input type="text" class="form-control" name="mail_address" placeholder="Email" value="<?php echo isset($_COOKIE['mail_address']) ? $_COOKIE['mail_address'] : ''; ?>">
                    </div>
                </div>

                <div class="row">
                    <div class="input-group col-md-4 offset-md-4 input error">
                        <?php echo isset($error['mail_address']) ? $error['mail_address'] : ''; ?>
                    </div>
                </div>

                <div class="row">
                    <div class="input-group col-md-4 offset-md-4 input">
                        <input type="password" class="form-control" name="password" placeholder="Password" value="<?php echo isset($_COOKIE['password']) ? $_COOKIE['password'] : ''; ?>">
                    </div>
                </div>

                <div class="row">
                    <div class="input-group col-md-4 offset-md-4 input error">
                        <?php echo isset($error['password']) ? $error['password'] : ''; ?>
                    </div>
                </div>

                <div class="row">
                    <div class="input-group col-md-4 offset-md-4 input" style="">
                        <button type="submit" name="login_action" class="btn btn-primary" style="width: 100%">Login</button>
                        <div class="form-check" style="margin-top: 10px">
                            <label class="form-check-label">
                                <input type="checkbox" name="remember_me" class="form-check-input" <?php echo isset($_COOKIE['mail_address']) ? 'checked' : ''; ?> >
                                Remember Me
                            </label>
                        </div>
                    </div>
                </div>     
            </div>
        </form>
    </body>
</html>