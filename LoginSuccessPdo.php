<?php
    session_start();
?>
<html>
    <head>
        <title>LoginSuccessPdo</title>
    </head>
    <body>
        Đăng nhập thành công</br>
        <?php
            if (isset($_SESSION['mail_address'])) {
                echo 'Email là: ' . $_SESSION['mail_address'];
            } else {
                echo 'Không tồn tài session';
            }
        ?>
        <form action="LoginSuccessPdo.php" method="GET">
            <input type="submit" name="logout" value="Logout">
            <?php
                if (isset($_GET['logout'])) {
                    unset($_SESSION['mail_address']);
                    unset($_SESSION['password']);
                    header("Location: LoginPdo.php");
                }
            ?>
        </form>
    </body>
</html>