<?php
    session_start();
?>
<html>
    <head>
    	<title>LoginSuccess</title>
    </head>
    <body>
    	Đăng nhập thành công</br>	
    	<?php
            if (isset($_COOKIE['email']) && isset($_COOKIE['password'])) {
                echo 'Có tồn tại cookie';
            } else {
                echo 'Không tồn tại cookie';
            }

            if (isset($_SESSION['email']) && isset($_SESSION['password'])) {
                echo '</br>Có tồn tại session';
            } else {
                echo '</br>Không tồn tại session';
            }
        ?>
        <form action="LoginSuccess.php" method="GET">
        	<input type="submit" name="logout" value="Logout">
        	<?php
    			if (isset($_GET['logout'])) {
    				unset($_SESSION['email']);
    				unset($_SESSION['password']);
    				header("Location: Login.php");
    			}
    		?>
        </form>
    </body>
</html>