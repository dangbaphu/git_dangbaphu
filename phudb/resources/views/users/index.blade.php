@extends('layouts.default')
@section('title', __('Danh sách người dùng'))
@section('content')

<body>
    <form method="GET" action="{{ route('users.index') }}">
        <label>Địa chỉ email</label><input type="text" name="email" value="">
        <label>Tên</label><input type="text" name="name" value="">
        <label>Địa chỉ</label><input type="text" name="address" value="">
        <label>Số điện thoại</label><input type="text" name="phone" value=""><button type="submit">Tìm kiếm</button>
    </form>
    <div class="container">
            @if (session('status'))
                <div class="alert alert-info">{{session('status')}}</div>
            @endif
        <table class="table table-bordered" style="margin-top: 20px">
            <thead style="text-align: center">
                <tr>
                    <th>STT</th>
                    <th>Địa chỉ email</th>
                    <th>Tên</th>
                    <th>Địa chỉ</th>
                    <th>Số điện thoại</th>
                </tr>
            </thead>
            <tbody>
                <?php $i = ($users->currentpage()-1)*$users->perpage() +1; ?>
                @foreach($users as $user)
                    <tr>
                        <th>{{ $i++ }}</th>
                        <th>{{ $user-> mail_address }}</th>
                        <th>{{ Helper::toUpperCase($user->name) }}</th>
                        <th>{{ $user-> address }}</th>
                        <th>{{ $user-> phone }}</th>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{ $users->links() }}
        <a href="{{ route('users.create') }}" class="button">Thêm mới User</a>
    </div>
@endsection
