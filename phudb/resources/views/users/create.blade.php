@extends('layouts.default')
@section('title', __('Thêm mới người dùng'))
@section('content')
<body>
    <form action="{{ route('users.store') }}" method="POST" >
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
        <div class="container">
            <div class="row">
                <div class="input-group col-md-6 offset-md-5" style="margin-top: 10px">
                    <h3>Thêm mới</h3>
                </div>
            </div>
            <div class="row">
                <div class="input-group col-md-6 offset-md-4" style="">
                    <table>
                        <tr>
                            <td class="@if ($errors->has('mail_address')) {{'error1'}} @endif">Địa chỉ email</td>
                            <td><input type="text" class="form-control @if ($errors->has('mail_address')) {{'error2'}} @endif" name="mail_address"></td>
                        </tr>
                        <tr>
                            <td colspan="2" class="error1">
                                {{ $errors->first('mail_address') }}
                            </td>
                        </tr>

                        <tr>
                            <td class="@if ($errors->has('password')) {{'error1'}} @endif">Mật khẩu</td>
                            <td><input type="password" class="form-control @if ($errors->has('password')) {{'error2'}} @endif" name="password"></td>
                        </tr>

                        <tr>
                            <td colspan="2" class="error1">
                                {{ $errors->first('password') }}
                            </td>
                        </tr>

                        <tr>
                            <td class="@if ($errors->has('password_confirmation')) {{'error1'}} @endif">Mật khẩu xác nhận</td>
                            <td><input type="password" class="form-control @if ($errors->has('password_confirmation')) {{'error2'}} @endif" name="password_confirmation"></td>
                        </tr>

                        <tr>
                            <td colspan="2" class="error1">
                                {{ $errors->first('password_confirmation') }}
                            </td>
                        </tr>

                        <tr>
                            <td class="@if ($errors->has('name')) {{'error1'}} @endif">Tên</td>
                            <td><input type="text" class="form-control @if ($errors->has('name')) {{'error2'}} @endif" name="name"></td>
                        </tr>

                        <tr>
                            <td colspan="2" class="error1">
                                {{ $errors->first('name') }}
                            </td>
                        </tr>

                        <tr>
                            <td class="@if ($errors->has('address')) {{'error1'}} @endif">Địa chỉ</td>
                            <td><input type="text" class="form-control @if ($errors->has('address')) {{'error2'}} @endif" name="address"></td>
                        </tr>

                        <tr>
                            <td colspan="2" class="error1">
                                {{ $errors->first('address') }}
                            </td>
                        </tr>

                        <tr>
                            <td class="@if ($errors->has('phone')) {{'error1'}} @endif">Số điện thoại</td>
                            <td><input type="text" class="form-control @if ($errors->has('phone')) {{'error2'}} @endif" name="phone"></td>
                        </tr>

                        <tr>
                            <td colspan="2" class="error1">
                                {{ $errors->first('phone') }}
                            </td>
                        </tr>
                        
                        <tr>
                            <td></td>
                            <td><button type="submit" href="{{ url('users/') }}" name="create_action" class="btn btn-primary" style="width: 100%">Register</button></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </form>
@endsection
