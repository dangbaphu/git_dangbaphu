<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('vi_VN');

        $limit = 100;

        for ($i = 0; $i < $limit; $i++) {
            DB::table('users')->create([
                'mail_address' => $faker->unique()->email,
                'password' => Hash::make($faker->password),
                'name' => $faker->name,
                'address'=> $faker->address,
                'phone' => $faker->e164PhoneNumber,
            ]);
        }
    }
}
