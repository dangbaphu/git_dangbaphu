<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class User extends Model
{
    protected $fillable = [
        'mail_address', 'password', 'name', 'address', 'phone',
    ];

    public function getUsers ()
    {
        return User::orderBy('mail_address', 'ASC')->paginate(20);
    }

    public function addUsers($data)
    {
    	$data['password'] = bcrypt($data['password']);
        return User::create($data);
    }

    public function findUsers($data)
    {

        return User::where('mail_address', 'like', '%'.$data['email'].'%')
        ->where('name', 'like', '%'.$data['name'].'%')
        ->where('address', 'like', '%'.$data['address'].'%')
        ->where('phone', $data['phone'])
        ->paginate(20);
    }

}
