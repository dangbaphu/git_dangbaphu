<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Login</title>
        <style>
            .error{
                color: red;
            }
        </style>
    </head>
    <body>
        <?php

        $error = array();
        $data = array();
        $success = array();
        if (!empty($_POST['login_action'])) {
            $data['email'] = isset($_POST['email']) ? $_POST['email'] : '';
            $data['password'] = isset($_POST['password']) ? $_POST['password'] : '';

            if (empty($data['email'])) {
                $error['email'] = 'Bạn chưa nhập tên';
            } else if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
                $error['email'] = 'Email không đúng định dạng';
            } else if (strlen($data['email']) > 255) {
                $error['email'] = 'Email không được quá 255 ký tự';
            }

            if (empty($data['password'])) {
                $error['password'] = 'Bạn chưa nhập password';
            } else if (strlen($data['password']) < 6 || strlen($data['password']) >50) {
                $error['password'] = 'Độ dài PassWord chỉ từ 6 đến 50 ký tự';
            }

            if ( $data['email'] == 'phudb@gmail.com' && $data['password'] == 'phudb123') {
                $success['login'] = 'Đăng nhập thành công';
                session_start();
                $_SESSION['email'] = $data['email'];
                $_SESSION['password'] = $data['password'];
                if (isset($_POST['remember_me'])) {
                    setcookie("email", $data['email'], time() + 3600*24*100);
                    setcookie("password", $data['password'], time() + 3600*24*100);
                    header("Location: LoginSuccess.php");
                } else {
                    if (isset($_COOKIE['email']) && isset($_COOKIE['password'])) {
                        setcookie("email", "", time() - 3600*24*100);
                        setcookie("password", "", time() - 3600*24*100);
                        header("Location: LoginSuccess.php");
                    }
                    header("Location: LoginSuccess.php");
                }
            } else {
                $error['login'] = 'Đăng nhập thất bại';
            }
        }
        ?>

        <form method="POST" action="Login.php">
            <table>
                <tr>
                    <td>Email</td>
                    <td><input type="text" name="email" value="<?php echo isset($_COOKIE['email']) ? $_COOKIE['email'] : ''; ?>"></td>
                    <td class="error"><?php echo isset($error['email']) ? $error['email'] : ''; ?></td>
                </tr>
                <tr>
                    <td>PassWord</td>
                    <td><input type="password" name="password" value="<?php echo isset($_COOKIE['password']) ? $_COOKIE['password'] : ''; ?>"></td>
                    <td class="error"><?php echo isset($error['password']) ? $error['password'] : ''; ?></td>
                </tr>
                <tr>
                    <td colspan="2">       
                        <?php echo isset($success['login']) ? $success['login'] : ''; ?>            
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="error">       
                        <?php echo isset($error['login']) ? $error['login'] : ''; ?>           
                    </td>
                </tr>
            </table>
            <input type="submit" name="login_action" value="Đăng nhập">
            <input type="checkbox" name="remember_me" <?php echo isset($_COOKIE['email']) ? 'checked' : ''; ?>  >Remember Me
        </form>
    </body>
</html>