<?php
abstract class Supervisor implements Boss
{
    protected $slogan;

    abstract public function saySloganOutLoud();

    public function setSlogan($string)
    {
        $this->slogan = $string;
    }            
}

interface Boss
{
    public function checkValidSlogan();
}

class EasyBoss extends Supervisor 
{
    use Active;

    public function saySloganOutLoud()
    {
        echo $this->slogan;
    }

    public function checkValidSlogan()
    {
        if (preg_match('/(after)/', $this->slogan, $maches) || preg_match('/(before)/', $this->slogan, $maches)) {                
            return true;
        } else {
            return false;
        }
    }
}
class UglyBoss extends Supervisor
{
    use Active;

    public function saySloganOutLoud()
    {
        echo $this->slogan;
    }
    
    public function checkValidSlogan()
    {
        if (preg_match('/(after)/', $this->slogan, $maches) && preg_match('/(before)/', $this->slogan, $maches)) {                
            return true;
        } else {
            return false;
        }
    }
}

trait Active
{
    public function defindYourSelf()
    {
        return get_class($this);
    }
}   

$easyBoss = new EasyBoss();
$uglyBoss = new UglyBoss();

$easyBoss->setSlogan('Do NOT push anything to master branch before reviewed by supervisor(s)');

$uglyBoss->setSlogan('Do NOT push anything to master branch before reviewed by supervisor(s). Only they can do it after check it all!');

$easyBoss->saySloganOutLoud(); 
echo "<br>";
$uglyBoss->saySloganOutLoud(); 
echo "<br>";
var_dump($easyBoss->checkValidSlogan()); // true
echo "<br>";
var_dump($uglyBoss->checkValidSlogan()); // true
echo "<br/>";
echo 'I am ' . $easyBoss->defindYourSelf(); 
echo "<br>";
echo 'I am ' . $uglyBoss->defindYourSelf(); 
?>