<?php 
    include_once './vendor/autoload.php';
    try {
    $conn = new PDO(
        'mysql:host=localhost;dbname=phudb;charset=utf8',
        'root',
        ''
    );

    } catch (PDOException $ex) {
        echo 'Ket noi that bai';
    }
?>
<html>
<head>
    <title>RegisterPdo</title>
    <link rel="stylesheet" type="text/css" href="vendor/twbs/bootstrap/dist/css/bootstrap.min.css">
    <style>
        .input {
            margin-top: 20px;
        }

        .error {
            color: red;
        }
    </style>
</head>
<body>
    <?php
        $error = array();
        $data = array();
        if (isset($_POST['login_action'])) {
            $data['mail_address'] = isset($_POST['mail_address']) ? $_POST['mail_address'] : '';
            $data['password'] = isset($_POST['password']) ? $_POST['password'] : '';
            $data['password_confirm'] = isset($_POST['password_confirm']) ? $_POST['password_confirm'] : '';

            if (empty($data['mail_address']) && !strlen($data['mail_address'])) {
                $error['mail_address'] = 'Bạn chưa nhập email';
            } else if (!filter_var($data['mail_address'], FILTER_VALIDATE_EMAIL)) {
                $error['mail_address'] = 'Email không đúng định dạng';
            } else if (strlen($data['mail_address']) > 255) {
                $error['mail_address'] = 'Độ dài email từ 6 đến 50 ký tự';
            }

            if (empty($data['password'])) {
                $error['password'] = 'Bạn chưa nhập password';
            } else if (strlen($data['password']) < 6 || strlen($data['password']) > 50) {
                $error['password'] = 'Độ dài password từ 6 đến 50 ký tự';
            } else if ($data['password_confirm'] != $data['password']) {
                $error['password_confirm'] = 'Password không trùng khớp';
            }

            if (!isset($error['mail_address']) && !isset($error['password']) && !isset($error['password_confirm'])) {
                $query = "SELECT mail_address FROM users WHERE mail_address = :mail_address";
                $stmt = $conn->prepare($query);
                $stmt->bindValue(':mail_address', $data['mail_address']);
                $stmt->execute();
                $user = $stmt->fetch(PDO::FETCH_ASSOC);
                if ($user) {
                    $error['mail_address'] = 'Email đã được sử dụng';
                } else {
                    $created_at = strtotime("now");
                    $data['password'] = md5($data['password']);
                    $query = $conn->prepare('
                            INSERT INTO users (mail_address, password, created_at)
                            VALUES (:mail_address, :password, :created_at)
                        ');
                    $query->bindParam(':mail_address', $data['mail_address']);
                    $query->bindParam(':password', $data['password']);
                    $query->bindParam(':created_at', $created_at);
                    $query->execute();
                    echo "Đăng ký thành công";
                }
            }
        }
    ?>
    <form method="POST" action="">
        <div class="container">
            <div class="row">
                <div class="input-group col-md-4 offset-md-4" style="margin-top: 100px">
                    <input type="text" class="form-control" name="mail_address" placeholder="Email">
                </div>
            </div>

            <div class="row">
                <div class="input-group col-md-4 offset-md-4 input error">
                    <?php echo isset($error['mail_address']) ? $error['mail_address'] : ''; ?>
                </div>
            </div>

            <div class="row">
                <div class="input-group col-md-4 offset-md-4 input">
                    <input type="password" class="form-control" name="password" placeholder="Password">
                </div>
            </div>

            <div class="row">
                <div class="input-group col-md-4 offset-md-4 input error">
                    <?php echo isset($error['password']) ? $error['password'] : ''; ?>
                </div>
            </div>

            <div class="row">
                <div class="input-group col-md-4 offset-md-4 input">
                    <input type="password" class="form-control" name="password_confirm" placeholder="Password Confirm">
                </div>
            </div>

            <div class="row">
                <div class="input-group col-md-4 offset-md-4 input error">
                    <?php echo isset($error['password_confirm']) ? $error['password_confirm'] : ''; ?>
                </div>
            </div>

            <div class="row">
                <div class="input-group col-md-4 offset-md-4 input" style="">
                    <button type="submit" name="login_action" class="btn btn-primary" style="width: 100%">Register</button>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
